//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    imgUrls: [
      '../../images/present.jpg',
      '../../images/present1.jpg',
      '../../images/present2.jpg'
    ],
    indicatorDots: true,
    autoplay: true,
    circular: true,
    vertical: false,
    interval: 5000,
    duration: 1000,
    presentList:[]
  },
  onLoad:function(){
    var that = this;
    wx.request({
      url: 'https://easy-mock.com/mock/5d3faeb94fed767e031fa0bb/example/PresentList',
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        var data = res.data.data;
        that.setData({
          presentList: data
        });
      }
    })
  }
})
